//
//  JVSettingsViewModel.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 01/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVSettingsViewModel: NSObject {
    private enum JVSettingsItem: Int {
        case UseLightContent
        case UseNetworkData
        case DestroyData
        
        var title: String? {
            switch rawValue {
            case 0:
                return "lb_use_light_content".localized()
            case 1:
                return "lb_use_4G".localized()
            default:
                return nil
            }
        }
        
        var identifier: String {
            switch rawValue {
            case 0:
                return UserDefaults.Constants.UseLightContent
            case 1:
                return UserDefaults.Constants.UseNetworkData
            default:
                return ""
            }
        }
        
        var boolValue: Bool {
            return identifier.userDefaultBoolValue()
        }
        
        func flipValue() {
            guard self == .UseLightContent || self == .UseNetworkData else { return }
            UserDefaults.standard.set(!boolValue, forKey: identifier)
        }
    }
    
    func titleForItemAt(index: Int) -> String? {
        return JVSettingsItem(rawValue: index)?.title
    }
    
    func switchValueForItemAt(index: Int) -> Bool {
        return JVSettingsItem(rawValue: index)?.boolValue ?? false
    }
    
    func changedSwitchAt(index: Int, newValue: Bool) {
        JVSettingsItem(rawValue: index)?.flipValue()
    }
    
    func cellSelectedAt(index: Int) {
        guard let settingsItem = JVSettingsItem(rawValue: index), settingsItem == .DestroyData else { return }
        
        UIAlertController.controllerWith(title: "alert_clear_all_content".localized(), okCallback: { (_) in
            JVCoreDataManager.instance.dropAll()
            "System cleared".displayToUser()
        })
    }
}
