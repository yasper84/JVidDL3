//
//  JVSettingsVC.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVSettingsVC: JVHomeChildVC {

    private struct Constants {
        static let ItemHeight: CGFloat = 65
    }

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var flowLayout: UICollectionViewFlowLayout!
    
    private let viewModel = JVSettingsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
    }
    
    private func configureCollectionView() {
        [JVSettingsSwitchCell.self, JVResetCell.self].forEach({
            collectionView.register(UINib(nibName: $0.identifier, bundle: nil),
                                    forCellWithReuseIdentifier: $0.identifier)
        })
        
        flowLayout.minimumLineSpacing = 0
        let inset: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
        flowLayout.estimatedItemSize = CGSize(width: collectionView.bounds.size.width - inset,
                                              height: Constants.ItemHeight)
    }
 }

extension JVSettingsVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.cellSelectedAt(index: indexPath.item)
    }
}

extension JVSettingsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = (0...1 ~= indexPath.item) ? JVSettingsSwitchCell.identifier : JVResetCell.identifier
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        if let switchCell = cell as? JVSettingsSwitchCell {
            switchCell.settingsLabel.text = viewModel.titleForItemAt(index: indexPath.item)
            switchCell.valueSwitch.isOn = viewModel.switchValueForItemAt(index: indexPath.item)
            
            switchCell.onSwitchValueChanged = { [weak self] (value) in
                self?.viewModel.changedSwitchAt(index: indexPath.item, newValue: value)
            }
        }
        return cell
    }
}
