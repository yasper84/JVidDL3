//
//  JVSettingsSwitchCell.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 01/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVSettingsSwitchCell: JVBaseCell {
    
    var onSwitchValueChanged: ((Bool) -> Void)?

    @IBOutlet weak var valueSwitch: UISwitch!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var rulerView: UIView!
    
    @IBAction func switchChanged(_ sender: Any) {
        onSwitchValueChanged?(valueSwitch.isOn)
    }
}
