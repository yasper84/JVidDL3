//
//  JVMenuItem.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

enum MenuItem: Int {
    case Library
    case Downloads
    case Search
    case Settings
    
    var title: String {
        switch self {
        case .Library:
            return "Library"
        case .Downloads:
            return "Downloads"
        case .Search:
            return "Search"
        case .Settings:
            return "Settings"
        }
    }
    
    var extendedTitle: String {
        switch self {
        case .Library:
            return "Library"
        case .Downloads:
            return "Active Downloads"
        case .Search:
            return "Search"
        case .Settings:
            return "Settings"
        }
    }
    
    var image: UIImage {
        switch self {
        case .Library:
            return #imageLiteral(resourceName: "libraryIcon")
        case .Downloads:
            return #imageLiteral(resourceName: "downloadsIcon")
        case .Search:
            return #imageLiteral(resourceName: "searchIcon")
        case .Settings:
            return #imageLiteral(resourceName: "settingsIcon")
        }
    }
}
