//
//  JVHomeChild.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 11/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

protocol JVHomeChild {
    func configureNavBar(_ navBar: UINavigationBar)
}
