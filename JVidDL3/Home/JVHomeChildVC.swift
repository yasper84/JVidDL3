//
//  JVHomeChildVC.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 11/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVHomeChildVC: JVViewController {
    func configureNavItem(_ navItem: UINavigationItem) {
        navItem.leftBarButtonItems = nil
        navItem.rightBarButtonItems = nil
    }
}
