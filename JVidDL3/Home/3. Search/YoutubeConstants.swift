//
//  YoutubeConstants.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 11/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

enum YoutubeConstants {
    // http://www.genyoutube.net/formats-resolution-youtube-videos.html
    // Audio only keys
    static let PreferredAudioQuality = [
        141,   //Audio 256 KBPS
        172,   //44.1 KHz 256 Kbps
        140,   //Audio 128 KBPS
        171]   //Audio: Stereo, 44.1 KHz 128 Kbps

    // Video only keys
    static let PreferredVideoQuality = [
        22,  //1280 x 720
        35,  //854 x 480
        37,  //1920 x 1080
        18,  //640 x 360
        34,  //640 x 360
        37]  //4096 x 3072
}
