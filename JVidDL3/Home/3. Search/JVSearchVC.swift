//
//  JVSearchVC.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import WebKit

final class JVSearchVC: JVHomeChildVC {
    
    private struct Constants {
        static let InitialURL = URL(string: "https://www.youtube.com")!
    }
    
    private lazy var backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "backButton"), style: .done, target: self, action: #selector(backPressed))
    private lazy var nextButton = UIBarButtonItem(image: #imageLiteral(resourceName: "nextButton"), style: .done, target: self, action: #selector(nextPressed))
    private lazy var homeButton = UIBarButtonItem(image: #imageLiteral(resourceName: "homeIcon"), style: .done, target: self, action: #selector(homePressed))
    private lazy var saveButton = UIBarButtonItem(image: #imageLiteral(resourceName: "saveButton"), style: .done, target: self, action: #selector(savePressed))

    private lazy var webView: WKWebView = {
        let config = WKWebViewConfiguration()
        config.allowsInlineMediaPlayback = true
        
        let webview = WKWebView(frame: .zero, configuration: config)
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.navigationDelegate = self
        
        view.addSubview(webview)
        webview.autoPinEdgesToSuperviewEdges()
        
        return webview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homePressed()
    }
    
    // MARK: NavigationItem determination
    override func configureNavItem(_ navItem: UINavigationItem) {
        navItem.leftBarButtonItems = [backButton, nextButton, homeButton]
        navItem.rightBarButtonItem = saveButton
    }
    
    @objc private func backPressed() {
        if webView.canGoBack {
            webView.goBack()
        } else {
            homePressed()
        }
    }
    
    @objc private func nextPressed() {
        guard webView.canGoForward else { return }
        webView.goForward()
    }
    
    @objc private func homePressed() {
        webView.load(URLRequest(url: Constants.InitialURL))
    }
    
    @objc private func savePressed() {
        guard   let url = webView.url,
                let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
                let videoID = components.queryItems?.first(where: { $0.name == "v" })?.value else {
                    "Unable to find a video to download".displayToUser()
                    return
        }
        
        indicateActivity = true
        JVDownloadManager.instance.downloadVideo(videoID: videoID,
                                                 downloadInitCallback: { [weak self] (error) in
            DispatchQueue.main.async {
                self?.indicateActivity = false
                
                guard let error = error else { return }
                
                switch error {
                case .alreadyExists:
                    (JVCoreDataManager.instance.videoWithID(videoID)?.videoManaged == true ?
                        "Video already exists in library" :
                        "Video is already downloading").displayToUser(title: "Warning")
                case .NoContent(let string):
                    string.displayToUser(title: "Error")
                case .RequestFailed(let string):
                    string.displayToUser(title: "Error")
                }
            }
        })
    }
}

extension JVSearchVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        indicateActivity = true
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        error.localizedDescription.displayToUser()
        indicateActivity = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        indicateActivity = false
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        error.localizedDescription.displayToUser()
        indicateActivity = false
    }
}
