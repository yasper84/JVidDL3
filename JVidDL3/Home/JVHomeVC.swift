//
//  JVHomeVC.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import PureLayout

class JVHomeVC: JVViewController {

    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet weak var underlineXPos: NSLayoutConstraint!
    
    private let viewControllers: [JVHomeChildVC] = [JVLibraryVC(), JVDownloadsVC(), JVSearchVC(), JVSettingsVC()]
    
    private var activeMenuItem: MenuItem = .Library {
        didSet {
            updateContent()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTabbar()
        activeMenuItem = .Library
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnderline),
                                               name: UIDevice.orientationDidChangeNotification, object: nil)
        
        JVDownloadManager.instance.cleanZombieVids()
    }
    
    private func initTabbar() {
        for (pos, tabView) in stackView.subviews.compactMap({ $0 as? JVTabView }).enumerated() {
            guard let curMenuItem = MenuItem(rawValue: pos) else { continue }
            
            tabView.configureWith(menuItem: curMenuItem)
            
            tabView.tag = pos
            tabView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabItemSelected)))
        }
    }
    
    @objc private func tabItemSelected(_ gesture: UITapGestureRecognizer) {
        guard let index = gesture.view?.tag, let targetMenuItem = MenuItem(rawValue: index) else { return }
        activeMenuItem = targetMenuItem
    }
    
    private func updateContent() {
        // Cleanup
        let animated = !contentView.subviews.isEmpty
        contentView.subviews.forEach({ $0.removeFromSuperview() })

        // Update title, viewcontroller and underline
        title = activeMenuItem.extendedTitle
        UIView.animate(withDuration: animated ? 0.3 : 0, animations: ({
            self.updateUnderline()
        }))

        let targetVC = viewControllers[activeMenuItem.rawValue]
        guard let targetSubview = targetVC.view else { return }
        contentView.addSubview(targetSubview)
        targetSubview.autoPinEdgesToSuperviewEdges()

        // Update the navbar for the active item
        targetVC.configureNavItem(navigationItem)
    }
    
    @objc private func updateUnderline() {
        self.underlineXPos.constant = stackView.arrangedSubviews[activeMenuItem.rawValue].center.x
        self.view.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
