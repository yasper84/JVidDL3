//
//  JVLibraryHeaderCell
//  JVidDL3
//
//  Created by Jasper Siebelink on 16/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVLibraryHeaderCell: JVReusableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var onDeletePressed: ((String) -> Void)?
    var onMorePressed: ((String) -> Void)?
    
    var playlistName: String?
    
    @IBAction func deletePresed(_ sender: Any) {
        guard let playlistName = playlistName else { return }
        onDeletePressed?(playlistName)
    }

    @IBAction func morePressed(_ sender: Any) {
        guard let playlistName = playlistName else { return }
        onMorePressed?(playlistName)
    }
}
