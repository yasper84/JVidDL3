//
//  JVLibraryCell.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 16/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

final class JVLibraryCell: JVBaseCell {
    
    @IBOutlet private(set) weak var thumbImageView: UIImageView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var durationLabel: UILabel!
    @IBOutlet private(set) weak var dateLabel: UILabel!
    
    var onDeleteVideo: ((String) -> Void)?
    var onMoveVideo: ((String) -> Void)?
    
    var videoID: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = ""
        durationLabel.text = ""
        dateLabel.text = ""
        
        layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction private func deletePressed(_ sender: Any) {
        guard let videoID = videoID else { return }
        onDeleteVideo?(videoID)
    }
    
    @IBAction private func movePressed(_ sender: Any) {
        guard let videoID = videoID else { return }
        onMoveVideo?(videoID)
    }
}
