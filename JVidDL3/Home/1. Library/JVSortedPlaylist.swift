//
//  JVSortedPlaylist
//  JVidDL3
//
//  Created by Jasper Siebelink on 27/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVSortedPlaylist: NSObject {
    let playlist: JVPlaylist
    private(set) var videos: [JVVideo] = []
    
    private var changeObserver: Any?
    var onVideosChanged: (() -> Void)?
    
    init(playlist: JVPlaylist) {
        self.playlist = playlist
        
        super.init()

        changeObserver = NotificationCenter.default.addObserver(forName: Notification.SystemNotifications.PlaylistMutated,
                                                                object: nil,
                                                                queue: OperationQueue.main) { [weak self] (notification) in
            guard let sendingPlaylist = notification.object as? JVPlaylist, playlist == sendingPlaylist else { return }
            self?.resort()
            self?.onVideosChanged?()
        }
        
        resort()
    }
    
    private func resort() {
        if let unsortedVideos = playlist.videos?.array as? [JVVideo] {
            videos = unsortedVideos.sorted(by: {
                guard let firstDate = $0.videoDate, let secondDate = $1.videoDate else { return false }
                return firstDate < secondDate
            })
        } else {
            videos = []
        }
    }
    
    var title: String {
        return String(format: "%@ (%d)", playlist.playlistName ?? "", videos.count)
    }
    
    deinit {
        guard let changeObserver = changeObserver else { return }
        NotificationCenter.default.removeObserver(changeObserver)
    }
}
