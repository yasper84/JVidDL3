//
//  JVLibraryVC.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import RxSwift

class JVLibraryVC: JVHomeChildVC, LibraryModelDelegate {
    
    private struct Constants {
        static let ItemHeight: CGFloat = 150
        static let HeaderSize: CGFloat = 45
        static let FooterSize: CGFloat = 1
        static let ItemSpacing: CGFloat = 6
        static let LineSpacing: CGFloat = 15
    }
    
    @IBOutlet private weak var collectionView: UICollectionView!    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var noVidsLabel: UILabel!
    
    private lazy var addPlaylistButton = UIBarButtonItem(image: #imageLiteral(resourceName: "plusIcon"), style: UIBarButtonItem.Style.done, target: self, action: #selector(addPlaylistPressed))

    private lazy var viewModel = JVLibraryViewModel(delegate: self)
    private var playlistObserver: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()
        
        playlistObserver = viewModel.observe(\.sortedPlaylists) { [weak self] (model, change) in
            self?.collectionView.reloadData()
        }        
    }
    
    override func configureNavItem(_ navItem: UINavigationItem) {
        navItem.leftBarButtonItems = nil
        navItem.rightBarButtonItem = addPlaylistButton
    }
    
    private func configureCollectionView() {
        collectionView.register(UINib(nibName: JVLibraryCell.identifier, bundle: nil),
                                forCellWithReuseIdentifier: JVLibraryCell.identifier)
        
        collectionView.register(UINib(nibName: JVLibraryHeaderCell.identifier, bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: JVLibraryHeaderCell.identifier)
            
        collectionView.register(UINib(nibName: JVLibraryFooterCell.identifier, bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                withReuseIdentifier: JVLibraryFooterCell.identifier)

        flowLayout.minimumInteritemSpacing = Constants.ItemSpacing
        flowLayout.minimumLineSpacing = Constants.LineSpacing
        flowLayout.headerReferenceSize = CGSize(width: view.width, height: Constants.HeaderSize)
        flowLayout.footerReferenceSize = CGSize(width: view.width, height: Constants.FooterSize)
        
        let cellSpaceAvailable = UIScreen.main.bounds.size.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing
        flowLayout.itemSize = CGSize(width: cellSpaceAvailable/2, height: Constants.ItemHeight)
    }
    
    @objc private func addPlaylistPressed() {
        UIAlertController.controllerWith(title: "Enter new playlist name", okCallback: { (alertController) in
            guard let playlistTitle = alertController.textFields?[0].text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
            self.viewModel.createPlaylist(name: playlistTitle)
        }, containsTextfield: true)
    }
    
    func onPlaylistMutated(_ playlist: JVPlaylist) {
        guard let index = viewModel.sortedPlaylists.firstIndex(where: { $0.playlist == playlist }) else { return }
        collectionView.reloadSections(IndexSet(integer: index))
    }
    
    deinit {
        playlistObserver?.invalidate()
    }
}

extension JVLibraryVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.videoClickedAt(indexPath: indexPath)
    }
}

extension JVLibraryVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: JVLibraryHeaderCell.identifier, for: indexPath) as! JVLibraryHeaderCell
            
            let sortedPlaylist = viewModel.sortedPlaylists[indexPath.section]
            headerCell.titleLabel.text = viewModel.playlistNameFor(section: indexPath.section)
            headerCell.playlistName = sortedPlaylist.playlist.playlistName
            headerCell.deleteButton.isHidden = viewModel.hideDeleteButton(section: indexPath.section)
            headerCell.moreButton.isHidden = !viewModel.hideMoreButton(section: indexPath.section)
            headerCell.moreButton.setImage(viewModel.moreButtonImage(section: indexPath.section), for: .normal)

            headerCell.onDeletePressed = { [weak self] (playlistName) in
                self?.viewModel.removePlaylist(playlistName: playlistName)
            }
            
            headerCell.onMorePressed = { [weak self] (playlistName) in
                guard let `self` = self else { return }
                
                guard let newIndex = self.viewModel.indexFor(playlistName: playlistName) else { return }
                var indicesToReload: [Int] = [newIndex]
                if let curSelectedPlaylist = self.viewModel.expandedPlaylist, let curIndex = self.viewModel.sortedPlaylists.firstIndex(of: curSelectedPlaylist) {
                    indicesToReload.append(curIndex)
                }
                
                self.viewModel.playlistMorePressed(playlistName: playlistName)
                self.collectionView.reloadSections(IndexSet(indicesToReload))
                
                // Scroll to top
                if let attributes = self.flowLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                                         at: IndexPath(item: 0, section: newIndex)) {
                    collectionView.setContentOffset(CGPoint(x: 0, y: attributes.frame.origin.y - collectionView.contentInset.top), animated: true)
                }
            }
            
            return headerCell
        } else {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: JVLibraryFooterCell.identifier, for: indexPath)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.sortedPlaylists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems(index: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: JVLibraryCell.identifier, for: indexPath)
        if let libraryCell = cell as? JVLibraryCell {
            libraryCell.thumbImageView.image = viewModel.imageFor(indexPath: indexPath)
            libraryCell.titleLabel.text = viewModel.videoNameFor(indexPath: indexPath)
            libraryCell.dateLabel.text = viewModel.dateRepresentationFor(indexPath: indexPath)
            libraryCell.videoID = viewModel.sortedPlaylists[indexPath.section].videos[indexPath.row].videoID
            
            libraryCell.onDeleteVideo = { [weak self] (videoID) in
                guard let index = self?.viewModel.playlistIndexForVideo(videoID: videoID) else { return }
                self?.viewModel.deleteVideo(videoID: videoID, callback: { [weak self] in
                    self?.collectionView.reloadSections(IndexSet(integer: index))
                })
            }
            
            libraryCell.onMoveVideo = { [weak self] (videoID) in
                self?.viewModel.moveVideoWith(videoID: videoID)
            }
        }
        return cell
    }
}
