//
//  JVLibraryViewModel
//  JVidDL3
//
//  Created by Jasper Siebelink on 26/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import RxSwift

protocol LibraryModelDelegate: AnyObject {
    func onPlaylistMutated(_ playlist: JVPlaylist)
}

final class JVLibraryViewModel: NSObject {
    struct Constants {
        static let MinNumVidsForExpand = 2
    }
    
    private let disposeBag = DisposeBag()

    weak var modelDelegate: LibraryModelDelegate?

    @objc private(set) dynamic var sortedPlaylists: [JVSortedPlaylist] = []
    
    private var playlistPlaying: JVSortedPlaylist?
    
    private(set) var expandedPlaylist: JVSortedPlaylist?

    // MARK: Initialization
    init(delegate: LibraryModelDelegate) {
        super.init()
        
        modelDelegate = delegate
        initializeDatasource()
    }
    
    private func initializeDatasource() {
        JVCoreDataManager.instance.playlists.asObservable().observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] (playlists) in
            guard let `self` = self else { return }
            
            // Filter deleted sorted playlists
            let currentPlaylists = self.sortedPlaylists.filter({ playlists.contains($0.playlist) })

            // Append new sorted playlists
            let newSortedPlaylists: [JVSortedPlaylist] = playlists.filter({ (playlist) -> Bool in
                return self.sortedPlaylists.first(where: { (sortedPlaylist) -> Bool in
                    return sortedPlaylist.playlist == playlist
                }) == nil
            }).compactMap({
                let newPlaylist = JVSortedPlaylist(playlist: $0)
                newPlaylist.onVideosChanged = { [weak self] in
                    self?.modelDelegate?.onPlaylistMutated(newPlaylist.playlist)
                }
                return newPlaylist
            })
            
            // Sort and set
            self.sortedPlaylists = (currentPlaylists + newSortedPlaylists).sorted(by: { (sortedPlaylist1, sortedPlaylist2) -> Bool in
                if sortedPlaylist1.playlist.unsortedList || sortedPlaylist2.playlist.unsortedList {
                    return sortedPlaylist2.playlist.unsortedList
                }
                
                return sortedPlaylist1.title < sortedPlaylist2.title
            })
        }).disposed(by: disposeBag)
    }
    
    // MARK: Video
    func videoClickedAt(indexPath: IndexPath) {
        let playlist: JVSortedPlaylist = sortedPlaylists[indexPath.section]
        playlistPlaying = playlist
        playlistPlaying?.videos[indexPath.item].playVideo(in: playlist)
    }
    
    
    func imageFor(indexPath: IndexPath) -> UIImage {
        guard let imageData = sortedPlaylists[indexPath.section].videos[indexPath.item].videoImage, let videoImage = UIImage(data: imageData) else {
            return UIImage(named: "placeholder")!
        }
        return videoImage
    }
    
    func dateRepresentationFor(indexPath: IndexPath) -> String? {
        return sortedPlaylists[indexPath.section].videos[indexPath.item].videoDate?.toString()
    }
    
    func videoNameFor(indexPath: IndexPath) -> String? {
        return sortedPlaylists[indexPath.section].videos[indexPath.item].videoName
    }
    
    func deleteVideo(videoID: String, callback: @escaping (() -> Void)) {
        UIAlertController.controllerWith(title: "alert_delete_title".localized(), message: nil, okName: "alert_delete_bt_yes".localized(), okCallback: { (_) in
            JVCoreDataManager.instance.removeVideoWithID(videoID)
            callback()
        })
    }
    
    func moveVideoWith(videoID: String) {
        JVCoreDataManager.instance.videos.value.first(where: { $0.videoID == videoID })?.selectFolder()
    }
    
    // Playlist
    func numberOfItems(index: Int) -> Int {
        let sortedPlaylist = sortedPlaylists[index]
        if expandedPlaylist == sortedPlaylist {
            return sortedPlaylist.videos.count
        } else {
            return min(sortedPlaylist.videos.count, Constants.MinNumVidsForExpand)
        }
    }
    
    func hideDeleteButton(section: Int) -> Bool {
        return sortedPlaylists[section].playlist.unsortedList
    }
    
    func hideMoreButton(section: Int) -> Bool {
        return sortedPlaylists[section].videos.count > Constants.MinNumVidsForExpand
    }
    
    func moreButtonImage(section :Int) -> UIImage? {
        return UIImage(named: (sortedPlaylists[section] == expandedPlaylist) ? "collapseIcon" : "moreButton")
    }
    
    func playlistNameFor(section: Int) -> String? {
        return sortedPlaylists[section].title
    }
   
    func indexFor(playlistName: String) -> Int? {
        return sortedPlaylists.firstIndex(where: { $0.playlist.playlistName == playlistName })
    }
    
    func playlistIndexForVideo(videoID: String) -> Int? {
        guard let playlist = JVCoreDataManager.instance.videos.value.first(where: { $0.videoID == videoID })?.playlist else { return nil }
        return sortedPlaylists.firstIndex(where: { $0.playlist == playlist })
    }
    
    func createPlaylist(name: String) {
        do {
             try JVCoreDataManager.instance.createPlaylist(name)
        } catch (JVCoreDataManager.CoreDataError.AlreadyExists) {
            "alert_playlist_already_exists".localized().displayToUser(title: "alert_playlist_error".localized())
        } catch (JVCoreDataManager.CoreDataError.InvalidDetails) {
            "alert_playlist_invalid_name".localized().displayToUser(title: "alert_playlist_error".localized())
        } catch { }
    }
    
    func removePlaylist(playlistName: String) {
        UIAlertController.controllerWith(title: "alert_delete_playlist_title".localized(), message: nil, okName: "alert_delete_bt_yes".localized(), okCallback: { (_) in
            JVCoreDataManager.instance.removePlaylist(playlistName: playlistName)
        })
    }
    
    func playlistMorePressed(playlistName: String) {
        let targetPlaylist = sortedPlaylists.first(where: { $0.playlist.playlistName == playlistName })
        if expandedPlaylist == targetPlaylist  {
            expandedPlaylist = nil
        } else {
            expandedPlaylist = targetPlaylist
        }
    }
}
