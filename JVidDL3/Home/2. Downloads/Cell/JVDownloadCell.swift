//
//  JVDownloadCell.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 14/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

protocol JVDownloadCellDelegate: AnyObject {
    func organiseLater(video: JVVideo)
    func chooseFolder(video: JVVideo)
}

class JVDownloadCell: JVBaseCell {
    
    // State views
    @IBOutlet weak var downloadingView: UIView!
    @IBOutlet weak var pauseButton: JVRoundedButton!

    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var chartView: JVChartView!
    @IBOutlet weak var progressLabel: UILabel!
    
    // Meta
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    private var video: JVVideo?
    
    weak var downloadCellDelegate: JVDownloadCellDelegate?
    
    // MARK: State observing
    private var stateObserver: NSKeyValueObservation? {
        willSet {
            stateObserver?.invalidate()
        }
    }
    
    private var progressObserver: NSKeyValueObservation? {
        willSet {
            progressObserver?.invalidate()
        }
    }
    
    private var completionObserver: AnyObject? {
        willSet {
            guard let completionObserver = completionObserver else { return }
            NotificationCenter.default.removeObserver(completionObserver)
        }
    }
    
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = .white
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
    }
    
    override func prepareForReuse() {
        stateObserver = nil
        progressObserver = nil
        completionObserver = nil
        progressLabel.text = nil
        chartView.percentage = 0
    }
    
    func configureWith(video: JVVideo) {
        self.video = video
        
        determineViewVisibility()
        
        titleLabel.text = video.videoName
        dateLabel.text = video.videoDate?.toString()
        
        // If we're still downloading this video
        guard video.videoDownloaded == false else { return }
        video.getURLTask({ [weak self] (task) in
            guard task != nil else {
                // For some reason we lost the session. Clean up
                video.abortDownload()
                return
            }
            
            // Progress tracking
            self?.progressObserver = task?.observe(\.countOfBytesReceived, options: [.new], changeHandler: { (task, value) in
                DispatchQueue.main.async {
                    let percentage = task.countOfBytesExpectedToReceive > 0 ? (CGFloat(task.countOfBytesReceived) / CGFloat(task.countOfBytesExpectedToReceive)) * 100 : 0
                    self?.progressLabel.text = String(format: "%.0f%%%", percentage)
                    self?.infoLabel.text = String(format: "%.0f MB", CGFloat(task.countOfBytesExpectedToReceive) / 1024 / 1024)
                    self?.chartView.percentage = Int(percentage)
                }
            })

            // State tracking
            self?.stateObserver = task?.observe(\.state, options: [.new], changeHandler: { (task, value) in
                self?.updateButtonState()
            })
            
            // Completion tracking
            NotificationCenter.default.addObserver(forName: Notification.SystemNotifications.DLCompleted, object: nil, queue: OperationQueue.main, using: { (notification) in
                guard let taskID = notification.object as? String, taskID == video.videoID else { return }
                self?.determineViewVisibility()
            })
        })

        updateButtonState()
    }
    
    private func updateButtonState() {
        video?.getURLTask({ [weak self] (task) in
            guard let task = task else { return }
            
            DispatchQueue.main.async {
                self?.pauseButton.setTitle((task.state == .suspended ? "bt_resume" : "bt_pause").localized(), for: .normal)
            }
        })
    }
    
    private func determineViewVisibility() {
        let downloadComplete = video?.videoDownloaded == true
        downloadingView.isHidden = downloadComplete
        completedView.isHidden = !downloadComplete
    }
    
    // MARK: Download button callbacks
    @IBAction func pausePressed(_ sender: Any) {
        let isPaused = pauseButton.title(for: .normal) == "bt_resume".localized()
        
        if !isPaused {
            video?.changeDownloadState(newState: .pause)
        } else {
            JVDownloadManager.instance.checkCanStartDownload { [weak self] (canStart, cannotStartMessage) in
                guard canStart else {
                    cannotStartMessage.displayToUser()
                    return
                }
                self?.video?.changeDownloadState(newState: .resume)
            }
        }
    }
    
    @IBAction func abordPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "alert_abort_title".localized(), message: nil, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "alert_abort_bt_no".localized(), style: .default) { (_) in })
        
        alertController.addAction(UIAlertAction(title: "alert_abort_bt_yes".localized(), style: .destructive) { (_) in
            self.video?.abortDownload()
        })
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Completion button callbacks
    @IBAction func chooseFolderPressed(_ sender: Any) {
        guard let video = video else { return }
        downloadCellDelegate?.chooseFolder(video: video)
    }
    
    @IBAction func laterPressed(_ sender: Any) {
        guard let video = video else { return }
        downloadCellDelegate?.organiseLater(video: video)
    }
    
    deinit {
        stateObserver?.invalidate()
        progressObserver?.invalidate()
        completionObserver = nil
        NotificationCenter.default.removeObserver(self)
    }
}
