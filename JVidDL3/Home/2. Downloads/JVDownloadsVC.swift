//
//  JVDownloadsVC.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class JVDownloadsVC: JVHomeChildVC {
    private struct Constants {
        static let LineSpacing: CGFloat = 15
        static let ItemSpacing: CGFloat = 6
        static let ItemHeight: CGFloat = 150
    }

    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let viewModel = JVDownloadsViewModel()
    
    fileprivate let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()
        
        JVCoreDataManager.instance.videos.asObservable().observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] (videos) in
            self?.collectionView.reloadSections(IndexSet(integer: 0))
        }).disposed(by: disposeBag)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(listUpdated),
                                               name: Notification.SystemNotifications.PlaylistMutated,
                                               object: nil)

        collectionView.reloadData()
    }
    
    @objc private func listUpdated() {
        collectionView.reloadData()
    }
    
    private func configureCollectionView() {
        collectionView.register(UINib(nibName: JVDownloadCell.identifier, bundle: nil), forCellWithReuseIdentifier: JVDownloadCell.identifier)

        flowLayout.minimumInteritemSpacing = Constants.ItemSpacing
        flowLayout.minimumLineSpacing = Constants.LineSpacing

        let cellSpaceAvailable = UIScreen.main.bounds.size.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing
        flowLayout.itemSize = CGSize(width: cellSpaceAvailable/2,
                                     height: Constants.ItemHeight)
    }
}

extension JVDownloadsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: JVDownloadCell.identifier, for: indexPath)
        if let downloadCell = cell as? JVDownloadCell, let cellViewModel = viewModel.videos[safe: indexPath.row] {
            downloadCell.configureWith(video: cellViewModel)
            downloadCell.downloadCellDelegate = self
        }
        return cell
    }
}

extension JVDownloadsVC: JVDownloadCellDelegate {
    func organiseLater(video: JVVideo) {
        guard let unsortedPlaylist = JVCoreDataManager.instance.playlists.value.first(where: { $0.unsortedList == true }) else { return }
        
        video.playlist = unsortedPlaylist
        video.markManaged()
        collectionView.reloadSections(IndexSet(integer: 0))
    }
    
    func chooseFolder(video: JVVideo) {
        video.selectFolder(folderSelected: { [weak self] in
            self?.collectionView.reloadSections(IndexSet(integer: 0))
        })
    }
}
