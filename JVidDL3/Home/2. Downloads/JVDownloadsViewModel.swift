//
//  JVDownloadsViewModel.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 14/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVDownloadsViewModel: NSObject {
    var videos: [JVVideo] {
        get {
            return JVCoreDataManager.instance.videos.value.filter({ $0.videoManaged == false })
        }
    }
}
