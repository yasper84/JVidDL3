//
//  JVDownloadManager.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 03/02/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import Foundation
import XCDYouTubeKit
import Reachability

final class JVDownloadManager: NSObject {
    
    struct Constants {
        fileprivate static let YoutubeAPIKey = "AIzaSyAoMwf93V7n6zH_bg_Fnf2005-qdojb0so" // Your API key
        fileprivate static let MetaURLString = "https://www.googleapis.com/youtube/v3/videos?key=%@&part=snippet&id=%@"
        static let NumSimultaneousDownloads = 3
    }
    
    private let reachability: Reachability? = try? Reachability()
    
    private(set) lazy var urlSession: URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "BackgroundSession")
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        session.reset {}
        return session
    }()
    
    private var networkObserver: NSKeyValueObservation?
    
    // MARK: Initialization
    static let instance = JVDownloadManager()
    private override init() {
        super.init()
        
        urlSession.configuration.sessionSendsLaunchEvents = true
        createVideoFolder()
        observe4GPause()
        
        if Constants.YoutubeAPIKey.isEmpty {
            fatalError("Please supply a valid Youtube API key")
        }
    }
    
    private func createVideoFolder() {
        guard let vidFolderPath = vidFolderPath else { return }
        try? FileManager.default.createDirectory(at: vidFolderPath,
                                                 withIntermediateDirectories: true,
                                                 attributes: nil)
    }
    
    var vidFolderPath: URL? {
        get {
            return try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("vids")
        }
    }
    
    private func observe4GPause() {
        // Observe the user setting
        networkObserver = UserDefaults.standard.observe(\.useNetworkData, options: [.new], changeHandler: { [weak self] (defaults, change) in
            self?.checkNetworkState()
        })
        
        // Observe the network state
        NotificationCenter.default.addObserver(forName: Notification.Name.reachabilityChanged, object: nil, queue: OperationQueue.main) { [weak self] (_) in
            self?.checkNetworkState()
        }
        try? reachability?.startNotifier()
    }
    
    private func checkNetworkState() {
        guard allowedToDownload == false else { return }
        urlSession.getAllTasks { (tasks) in
            tasks.filter({ $0.state != .suspended }).forEach({ $0.suspend() })
        }
    }
    
    var allowedToDownload: Bool {
        return !(UserDefaults.standard.useNetworkData == false && reachability?.connection == Reachability.Connection.cellular )
    }
    
    // MARK: Download flow
    func downloadVideo(videoID: String, downloadInitCallback: @escaping ((JVDownloadError?) -> Void)) {
        guard JVCoreDataManager.instance.videoExistsInLibrary(videoID: videoID) == false else {
            downloadInitCallback(.alreadyExists)
            return
        }
        
        // Step 1
        retrieveVideoURL(videoID: videoID) { [weak self] (urlResult) in
            switch urlResult {
            case .success(let url):
                // Step 2
                self?.retrieveVideoMetaInfo(videoID: videoID, completion: { (videoResult) in
                    switch videoResult {
                    case .success(let ytVideo):
                        // Step 3
                        self?.downloadVideoEntry(videoURL: url, videoID: videoID, videoMeta: ytVideo)
                        downloadInitCallback(nil)
                    case .failure(let error):
                        downloadInitCallback(error)
                    }
                })
            case .failure(let error):
                downloadInitCallback(error)
            }
        }
    }
    
    // MARK: Step 1: Video URL retrieval
    enum JVDownloadError: Error {
        case alreadyExists
        case RequestFailed(String)
        case NoContent(String)
    }
    
    enum JVCallResult<T> {
        case success(T)
        case failure(JVDownloadError)
    }
    
    private func retrieveVideoURL(videoID: String, completion: @escaping ((JVCallResult<URL>) -> Void)) {
        // Retrieve the available URLs for this video
        XCDYouTubeClient.default().getVideoWithIdentifier(videoID) { (video, error) in
            guard error == nil else {
                completion(.failure(.RequestFailed(error?.localizedDescription ?? "Unknown URL Retrieval Error")))
                return
            }
            
            // Find the best preferred audio quality
            guard let vidMetaInfo = video?.streamURLs,
                let targetKey = YoutubeConstants.PreferredVideoQuality.filter({ return vidMetaInfo.keys.contains($0) }).first,
                let targetURL = vidMetaInfo[targetKey] else {
                    completion(.failure(.NoContent("Did not find a suitable Video format")))
                    return
            }
            
            completion(JVCallResult.success(targetURL))
        }
    }
    
    // MARK: Step 2: Video meta retrieval
    private struct Feed: Decodable {
        let items: [MetaItem]
        
        struct MetaItem: Decodable {
            let snippet: YTVideo
        }
    }
    
    private func retrieveVideoMetaInfo(videoID: String, completion: @escaping ((JVCallResult<YTVideo>) -> Void)) {
        let request = URLRequest(url: URL(string: String(format: Constants.MetaURLString, Constants.YoutubeAPIKey, videoID))!)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                guard let data = data else {
                    completion(.failure(.RequestFailed(error?.localizedDescription ?? "Unknown Meta Error")))
                    return
                }
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
                let parseResult = try decoder.decode(Feed.self, from: data)
                guard let firstItem = parseResult.items.first?.snippet else {
                    completion(.failure(.NoContent("No Metadata Found")))
                    return
                }
                completion(.success(firstItem))
            } catch {
                completion(.failure(.RequestFailed(error.localizedDescription)))
            }
            }.resume()
    }
    
    // MARK: Step 3: Video entry creation and download
    private func downloadVideoEntry(videoURL: URL, videoID: String, videoMeta: YTVideo) {
        JVCoreDataManager.instance.createVideo(videoURL: videoURL, videoID: videoID, videoMeta: videoMeta)
        
        checkCanStartDownload { (canStart, _) in
            let task: URLSessionDownloadTask = self.urlSession.downloadTask(with: videoURL)
            task.taskDescription = videoID
            
            if canStart {
                task.resume()
            }
        }
    }
    
    func checkCanStartDownload(callback: @escaping ((Bool, String) -> Void)) {
        urlSession.getAllTasks { [weak self] (tasks) in
            let autoStart = tasks.filter({ $0.state == .running }).count < Constants.NumSimultaneousDownloads
            let errorMessage = self?.allowedToDownload == false ?
                "Cellular downloads disabled" :
                String(format: "Only %d simultaneous downloads possible", Constants.NumSimultaneousDownloads)
            callback(autoStart && self?.allowedToDownload == true, errorMessage)
        }
    }
    
    func cleanZombieVids() {
        guard let vidFolderPath = vidFolderPath else { return }
        let fileManager = FileManager.default
        do {
            // Clean up downloaded videos without playlist
            let downloadedDeadVideos: [JVVideo] = JVCoreDataManager.instance.videos.value
                .filter({ $0.playlist == nil })
            downloadedDeadVideos.forEach({ $0.delete() })

            // Clean up files not linked to CoreData
            let coreDataVideoIDs: [String] = JVCoreDataManager.instance.videos.value
                .compactMap({ $0.videoID })
            let diskVideoPaths: [URL] = try fileManager
                .contentsOfDirectory(at: vidFolderPath,
                                     includingPropertiesForKeys: nil)
            diskVideoPaths.forEach({
                let diskFileID = $0.lastPathComponent.replacingOccurrences(of: ".mp4",
                                                                           with: "")
                guard !coreDataVideoIDs.contains(diskFileID) else { return }
                try? fileManager.removeItem(at: $0)
            })
        } catch {
            print("Error while enumerating files \(vidFolderPath.path): \(error.localizedDescription)")
        }
    }
    
    deinit {
        urlSession.invalidateAndCancel()
        networkObserver?.invalidate()
    }
}

// MARK: URLSessionDelegate
extension JVDownloadManager: URLSessionDownloadDelegate, URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        // Resume the next paused task
        urlSession.getAllTasks { (tasks) in
            tasks.first(where: { $0.state == .suspended })?.resume()
        }
        
        // In case of failure, remove task and inform the user
        guard let httpResponse = downloadTask.response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
            if let taskID = downloadTask.taskDescription {
                JVCoreDataManager.instance.removeVideoWithID(taskID)
            }
            "Network error".displayToUser()
            return
        }
        
        guard   let taskID = downloadTask.taskDescription,
            let videoURL = JVCoreDataManager.instance.videoWithID(taskID)?.videoURL,
            let _ = try? FileManager.default.moveItem(at: location, to: videoURL) else {
                print("Failed to move video")
                return
        }
        
        // Inform the system we're done downloading
        NotificationCenter.default.post(name: Notification.SystemNotifications.DLCompleted, object: taskID)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let givenError = error {
            print("Task failure: \(String(describing: task.taskDescription)), error: \(givenError.localizedDescription)")
            
            // Cleanup the video
            guard let videoID = task.taskDescription else { return }
            JVCoreDataManager.instance.removeVideoWithID(videoID)
        }
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.backgroundCompletionHandler?()
            }
        }
    }
}
