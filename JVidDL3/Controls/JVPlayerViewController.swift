//
//  JVPlayerViewController.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 27/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import AVKit

private struct Constants {
    static let UpdateInterval: Int64 = 10
}

class JVPlayerViewController: AVPlayerViewController {
    
    private var observer: Any? {
        willSet {
            guard let curObserver = observer else { return }
            player?.removeTimeObserver(curObserver)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        JVCoreDataManager.instance.saveContext()
    }
    
    func addObserving( callback: @escaping ((Float64) -> Void)) {
        observer = player?.addPeriodicTimeObserver(forInterval: CMTime(value: Constants.UpdateInterval, timescale: 1),
                                                   queue: DispatchQueue.main) { [weak self]  (time) in
            guard let player = self?.player else { return }
            callback(CMTimeGetSeconds(player.currentTime()))
        }
    }
    
    deinit {
        observer = nil
    }
}
