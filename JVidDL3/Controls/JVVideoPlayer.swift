//
//  JVVideoPlayer.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 17/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit
import AVFoundation

class JVVideoPlayer: NSObject {
    
    static let instance = JVVideoPlayer()

    
    private(set) var playerViewController: JVPlayerViewController?
    private(set) var player: AVQueuePlayer?
    private(set) var video: JVVideo?
    
    private override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackground),
                                               name: UIApplication.willResignActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(becameActive),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }
    
    @objc private func moveToBackground() {
        playerViewController?.player  = nil
    }

    @objc private func becameActive() {
        playerViewController?.player  = player
    }
    
    func play(video: JVVideo,
              playlist: JVSortedPlaylist) {
        guard let videoIndex: Int = playlist.videos.firstIndex(where: { $0 == video }) else { return }
        
        let playlistVideos: [JVVideo]
        if videoIndex == 0 {
            playlistVideos = playlist.videos
        } else {
            playlistVideos = Array(playlist.videos[videoIndex...playlist.videos.count-1]) + Array(playlist.videos[0...videoIndex-1])
        }

        let playerItems: [AVPlayerItem] = playlistVideos.compactMap({
            guard let videoURL = $0.videoURL else { return nil }
            return AVPlayerItem(url: videoURL)
        })

        self.video = video
        
        // Initialize the player at the last known location
        let player: AVQueuePlayer = AVQueuePlayer(items: playerItems)
        
        // Only resume for longer videos
        if video.videoProgress > 60*10 {
            player.currentItem?.seek(to: CMTime(seconds: video.videoProgress, preferredTimescale: 1),
                                    completionHandler: nil)
        }

        let playerViewController = JVPlayerViewController()
        playerViewController.player = player
        
        // Add observing
        playerViewController.addObserving { (progress) in
            video.videoProgress = progress
        }
        
        // Start playback
        UIApplication.shared.keyWindow?.rootViewController?.present(playerViewController, animated: true, completion: {
            player.play()
        })
        
        self.playerViewController = playerViewController
        self.player = player
    }
    
    var currentURL: URL? {
        return (JVVideoPlayer.instance.player?.currentItem?.asset as? AVURLAsset)?.url
    }
}
