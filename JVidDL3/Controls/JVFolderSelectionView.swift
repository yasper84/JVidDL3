//
//  JVFolderSelectionView.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 16/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVFolderSelectionView: UIView {
    private let selectionCallback: ((JVPlaylist?) -> Void)
    private var picker: UIPickerView?
    private var toolbar: UIToolbar?
    private var topToolbarConstraint: NSLayoutConstraint?
    
    private lazy var datasource: [JVPlaylist] = {
        return JVCoreDataManager.instance.playlists.value.sorted(by: { (playlist1, playlist2) -> Bool in
            if playlist1.unsortedList || playlist2.unsortedList {
                return playlist1.unsortedList
            }
            
            guard let name1 = playlist1.playlistName, let name2 = playlist2.playlistName else { return false }
            return name1 < name2
        })
    }()
    
    class func presentWith(selectionCallback: @escaping ((JVPlaylist?) -> Void)) {
        guard !JVCoreDataManager.instance.playlists.value.isEmpty else {
            "There are no playlists in the system. Please add one".displayToUser()
            return
        }
        
        guard let pickerSuperview = UIApplication.shared.keyWindow else { return }
        let folderSelectionView = JVFolderSelectionView(selectionCallback: selectionCallback)
        pickerSuperview.addSubview(folderSelectionView)
        folderSelectionView.autoPinEdgesToSuperviewEdges()
    }

    init(selectionCallback: @escaping ((JVPlaylist?) -> Void)) {
        self.selectionCallback = selectionCallback
        super.init(frame: .zero)
        
        backgroundColor = UIColor.black.withAlphaComponent(0.85)
        
        createPicker()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createPicker() {
        // Create the toolbar
        let toolbar = UIToolbar(forAutoLayout: ())
        toolbar.barTintColor = .black
        toolbar.tintColor = .white
        toolbar.layer.borderColor = UIColor.white.cgColor
        toolbar.layer.borderWidth = 1
        
        let fixedSpacing = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpacing.width = 10
        let toolbarItems = [fixedSpacing,
                            UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelPressed)),
                            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                            UIBarButtonItem(title: "Select", style: .done, target: self, action: #selector(selectPressed)),
                            fixedSpacing]
        toolbar.items = toolbarItems
        addSubview(toolbar)

        topToolbarConstraint = toolbar.autoPinEdge(.top, to: .bottom, of: self)
        toolbar.autoPinEdge(.left, to: .left, of: self)
        toolbar.autoPinEdge(.right, to: .right, of: self)
        self.toolbar = toolbar
        
        
        // Create the picker
        let picker = UIPickerView()
        picker.backgroundColor = .black
        picker.tintColor = .white
        picker.dataSource = self
        picker.delegate = self
        addSubview(picker)
        
        picker.autoPinEdge(.top, to: .bottom, of: toolbar)
        picker.autoPinEdge(.left, to: .left, of: self)
        picker.autoPinEdge(.right, to: .right, of: self)
        self.picker = picker
        
        pickerVisible = true
    }
    
    var pickerVisible: Bool = false {
        didSet {
            guard let picker = picker, let toolbar = toolbar else { return }
            self.topToolbarConstraint?.constant = pickerVisible ? -(picker.height + toolbar.height) : 0
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            }) { (completed) in
                if self.pickerVisible == false {
                    self.removeFromSuperview()
                }
            }
        }
    }
    
    @objc private func cancelPressed() {
        pickerVisible = false
    }
    
    @objc private func selectPressed() {
        guard let picker = picker else { return }
        selectionCallback(datasource[picker.selectedRow(inComponent: 0)])
        pickerVisible = false
    }
}

extension JVFolderSelectionView: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return datasource.count
    }
}

extension JVFolderSelectionView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: datasource[row].playlistName ?? "Unknown",
                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}
