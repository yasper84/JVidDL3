//
//  JVVideo.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 15/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension JVVideo {
    enum DownloadState {
        case pause
        case resume
    }
    
    var videoURL: URL? {
        get {
            guard let videoID = videoID else { return nil }
            return JVDownloadManager.instance.vidFolderPath?.appendingPathComponent(videoID).appendingPathExtension("mp4")
        }
    }
    
    var videoDownloaded: Bool {
        get {
            guard let filePath = videoURL?.path else { return false }
            return FileManager.default.fileExists(atPath: filePath)
        }
    }
    
    // MARK: Data manipulation
    func markManaged() {
        videoManaged = true
        JVCoreDataManager.instance.saveContext()
        
        NotificationCenter.default.post(name: Notification.SystemNotifications.PlaylistMutated, object: self.playlist)
    }
    
    // MARK: State mutation
    func changeDownloadState(newState: DownloadState) {
        getURLTask({ (task) in
            switch newState {
            case .pause:
                task?.suspend()
            case .resume:
                task?.resume()
            }
        })
    }
    
    func abortDownload() {
        guard let videoID = videoID else { return }
        
        getURLTask({ (task) in
            task?.cancel()
            
            JVCoreDataManager.instance.removeVideoWithID(videoID)
        })
    }
    
    func delete() {
        guard let videoID = videoID else { return }
        JVCoreDataManager.instance.removeVideoWithID(videoID)
    }
    
    func getURLTask(_ callback: @escaping ((URLSessionTask?) -> Void)) {
        JVDownloadManager.instance.urlSession.getAllTasks { (tasks) in
            callback(tasks.first(where: { $0.taskDescription == self.videoID }))
        }
    }
    
    func selectFolder(folderSelected: (() -> Void)? = nil) {
        JVFolderSelectionView.presentWith( selectionCallback: { [weak self] (playlist) in
            guard let playlist = playlist else { return }
            
            // Notify the old playlist
            let curPlaylist = self?.playlist            
            self?.playlist = playlist
            self?.markManaged()

            if let oldPlaylist = curPlaylist {
                NotificationCenter.default.post(name: Notification.SystemNotifications.PlaylistMutated, object: oldPlaylist)
            }
            
            folderSelected?()
        })
    }
    
    func playVideo(in playlistPlaying: JVSortedPlaylist) {
        JVVideoPlayer.instance.play(video: self,
                                    playlist: playlistPlaying)
    }
}
