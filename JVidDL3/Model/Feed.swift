//
//  Feed.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 13/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

private struct Constants {
    static let DefaultThumbURL = URL(string: "http://videopromotion.club/assets/images/default-video-thumbnail.jpg")!
}

struct YTVideo: Decodable {
    let publishedAt: Date
    let title: String
    let thumbnails: [String: Thumbnail]
    
    struct Thumbnail: Decodable {
        let url: URL
    }
    
    var bestThumbnail: URL {
        get {
            guard   let bestKey = ["high", "medium", "default"].filter({ thumbnails.keys.contains($0) }).first,
                let bestURL = thumbnails[bestKey]?.url else {
                    return Constants.DefaultThumbURL
            }
            return bestURL
        }
    }
}
