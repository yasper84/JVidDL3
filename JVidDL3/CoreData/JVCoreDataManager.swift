//
//  JVCoreDataManager.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 13/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import CoreData
import RxSwift
import RxCocoa

final class JVCoreDataManager: NSObject {
    private struct Constants {
        static let UnsortedPlaylist = "Unsorted videos"
    }
    
    static let instance = JVCoreDataManager()
    
    var playlists: BehaviorRelay<[JVPlaylist]> = BehaviorRelay(value: [])
    var videos: BehaviorRelay<[JVVideo]> = BehaviorRelay(value: [])
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            guard let error = error as NSError? else { return }
            fatalError("Unresolved error \(error), \(error.userInfo)")
        })
        return container
    }()
    
    // MARK: Initialization
    private override init() {
        super.init()
        
        retrievePlaylists()
        checkToCreateUnsortedPlaylist()
        retrieveVideos()
    }
    
    private func checkToCreateUnsortedPlaylist() {
        if playlists.value.first(where: { $0.playlistName == Constants.UnsortedPlaylist }) == nil {
            try? createPlaylist(Constants.UnsortedPlaylist, isUnsortedList: true)
        }
    }
    
    // MARK: CoreData manipulation
    func saveContext () {
        DispatchQueue.main.async {
            let context = self.persistentContainer.viewContext
            guard context.hasChanges else { return }
            
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func dropAll() {
        let items = [playlists.value, videos.value].compactMap({ return $0 as? NSManagedObject })
        items.forEach({
            persistentContainer.viewContext.delete($0)
        })
        saveContext()
        
        playlists.accept([])
        videos.accept([])
    }
    
    enum CoreDataError: Error {
        case AlreadyExists
        case InvalidDetails
    }
    
    // MARK: Content creation
    func createPlaylist(_ playlistName: String, isUnsortedList: Bool = false) throws {
        if !isUnsortedList {
            guard playlistName.count > 0  else {
                throw CoreDataError.InvalidDetails
            }
        
            guard !JVCoreDataManager.instance.containsPlaylist(title: playlistName) else {
                throw CoreDataError.AlreadyExists
            }
        }
        
        let entry = JVPlaylist(context: persistentContainer.viewContext)
        entry.playlistName = playlistName
        entry.unsortedList = isUnsortedList
        saveContext()
        
        var result = playlists.value
        result.append(entry)
        playlists.accept(result)
    }
    
    func createVideo(videoURL: URL, videoID: String, videoMeta: YTVideo) {
        let video = JVVideo(context: persistentContainer.viewContext)
        video.videoID = videoID
        video.videoDate = videoMeta.publishedAt
        video.videoName = videoMeta.title
        video.videoImage = try? Data(contentsOf: videoMeta.bestThumbnail)
        
        saveContext()
        
        var result = videos.value
        result.append(video)
        videos.accept(result)
    }
    
    // MARK: Content retrieval
    private enum ContentType {
        case Playlist
        case Video
    }
    
    func retrievePlaylists() {
        if let playlists = fetchItems(.Playlist) as? [JVPlaylist] {
            self.playlists.accept(playlists)
        }
    }
    
    func containsPlaylist(title: String) -> Bool {
        return playlists.value.filter({ $0.playlistName == title }).isEmpty == false
    }
    
    func retrieveVideos() {
        if let videos = fetchItems(.Video) as? [JVVideo] {
            self.videos.accept(videos)
        }
    }
    
    func videoWithID(_ videoID: String) -> JVVideo? {
        return videos.value.first(where: { $0.videoID == videoID })
    }
    
    func removeVideoWithID(_ videoID: String) {
        guard let video = videoWithID(videoID) else { return }
        
        let playlist = video.playlist
        video.playlist = nil
        persistentContainer.viewContext.delete(video)
        saveContext()
        
        guard let vidIndex = videos.value.firstIndex(where: { $0.videoID == videoID }) else { return }
        var result = videos.value
        result.remove(at: vidIndex)
        videos.accept(result)
        
        guard let playlistToDeleteFrom = playlist else { return }
        NotificationCenter.default.post(name: Notification.SystemNotifications.PlaylistMutated,
                                        object: playlistToDeleteFrom)
    }
    
    func removePlaylist(playlistName: String) {
        guard let playlist = playlists.value.first(where: { $0.playlistName == playlistName }) else { return }
        persistentContainer.viewContext.delete(playlist)
        
        guard let index = playlists.value.firstIndex(where: { $0 == playlist }) else { return }
        var result = playlists.value
        result.remove(at: index)
        playlists.accept(result)
    }
    
    private func fetchItems(_ itemType: ContentType) -> [NSManagedObject] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: (itemType == .Playlist ? "JVPlaylist" : "JVVideo"))
        request.returnsObjectsAsFaults = false
        do {
            guard let result = try persistentContainer.viewContext.fetch(request) as? [NSManagedObject] else {
                return []
            }
            return result
        } catch {
            return []
        }
    }
    
    // MARK: Meta
    func videoExistsInLibrary(videoID: String) -> Bool {
        return !videos.value.filter({ $0.videoID == videoID }).isEmpty
    }
}
