//
//  Notification+Extensions.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 16/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension Notification {
    struct SystemNotifications {
        static let DLCompleted = Notification.Name(rawValue: "DownloadCompleted")
        static let PlaylistMutated = Notification.Name(rawValue: "PlaylistMutated")
    }
}
