//
//  Date+Extensions.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 15/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension Date {
    static let SimpleDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter
    }()
    
    func toString() -> String {
        return Date.SimpleDateFormatter.string(from: self)
    }
}
