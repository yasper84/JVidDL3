//
//  UserDefaults+Extensions.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 02/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension UserDefaults {
    struct Constants {
        static let UseLightContent: String = "useLightContent"
        static let UseNetworkData: String = "useNetworkData"
    }
    
    @objc dynamic var useLightContent: Bool {
        return bool(forKey: Constants.UseLightContent)
    }
    
    @objc dynamic var useNetworkData: Bool {
        return bool(forKey: Constants.UseNetworkData)
    }
}
