//
//  String+Extensions.swift
//  JVidDL2
//
//  Created by Jasper Siebelink on 03/02/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension String {
    func displayToUser(title: String = "Info") {
        DispatchQueue.main.async {
            guard var navVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
            if let presentedNC = navVC.presentedViewController as? UINavigationController {
                navVC = presentedNC
            }
            
            let alert = UIAlertController(title: title, message: self, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            navVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func userDefaultBoolValue() -> Bool {
        return UserDefaults.standard.bool(forKey: self)
    }
}
