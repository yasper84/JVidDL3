//
//  UIView+Extensions.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension UIView {

    var width: CGFloat {
        return frame.size.width
    }

    var height: CGFloat {
        return frame.size.height
    }
}
