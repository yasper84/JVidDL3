//
//  Collection+Safe.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 26/05/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

extension Array {
    public subscript(safe index: Int) -> Element? {
        guard index >= 0, index < endIndex else {
            return nil
        }
        
        return self[index]
    }
}
