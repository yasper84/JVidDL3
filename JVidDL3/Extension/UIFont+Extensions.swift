//
//  JVFont.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension UIFont {
    class func projectFont(size: CGFloat = 10) -> UIFont {
        return UIFont(name: "ArialRoundedMTBold", size: size)!
    }
}
