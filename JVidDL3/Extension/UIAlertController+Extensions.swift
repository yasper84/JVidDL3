//
//  UIAlertController+Extensions.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 26/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

extension UIAlertController {
    class func controllerWith(title: String, message: String? = nil, okName: String = "OK", okCallback: ((UIAlertController) -> Void)? = nil, containsTextfield: Bool = false) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if containsTextfield {
            alertController.addTextField(configurationHandler: nil)
        }
        
        alertController.addAction(UIAlertAction(title: okName, style: .default) { (_) in
            okCallback?(alertController)
        })
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (_) in })
        
        alertController.show()
    }
    
    func show() {
        guard var navVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
        if let presentedNC = navVC.presentedViewController as? UINavigationController {
            navVC = presentedNC
        }

        DispatchQueue.main.async {
            navVC.present(self, animated: true, completion: nil)
        }
    }
}
