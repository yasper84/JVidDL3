//
//  JVLocalizedLabel.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 01/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

final class JVLocalizedLabel: JVStyleLabel {

    @IBInspectable var localizedString: String? {
        get { return nil }
        set( key ) {
            text = key?.localized()
        }
    }

}
