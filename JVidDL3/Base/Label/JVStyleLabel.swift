//
//  JVStyleLabel.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 02/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVStyleLabel: UILabel {
    
    @IBInspectable var invertColor: Bool = false
    private var styleObserver: NSKeyValueObservation?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        DispatchQueue.main.async {
            self.observeStyle()
        }
    }

    private func observeStyle() {
        styleObserver = UserDefaults.standard.observe(\.useLightContent, options: [.initial, .new], changeHandler: { [weak self] (defaults, change) in
            let targetColor: UIColor
            let useLightTheme = change.newValue == true
            if useLightTheme {
                targetColor = (self?.invertColor == true) ? .white : .black
            } else {
                targetColor = (self?.invertColor == true) ? .black : .white 
            }
            self?.textColor = targetColor
        })
    }
    
    deinit {
        styleObserver?.invalidate()
    }
}
