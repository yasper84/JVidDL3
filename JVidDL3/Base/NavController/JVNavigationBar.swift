//
//  JVNavigationBar.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 03/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVNavigationBar: UINavigationBar {

    private let ruler = UIView(forAutoLayout: ())
    
    private var styleObserver: NSKeyValueObservation?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)        
        commonInit()
    }
    
    private func commonInit() {
        DispatchQueue.main.async {
            self.addSubview(self.ruler)
            self.ruler.autoSetDimension(.height, toSize: 1)
            self.ruler.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
            
            (UIApplication.shared.delegate as? AppDelegate)?.rootNavController?.navigationBar.barStyle = .black;
            
            self.styleObserver = UserDefaults.standard.observe(\.useLightContent, options: [.initial, .new], changeHandler: { [weak self] (defaults, change) in
                let useLightTheme = change.newValue == true
                self?.updateStyle(useLightTheme: useLightTheme)
            })
        }
    }
    
    private func updateStyle(useLightTheme: Bool) {
        ruler.backgroundColor = useLightTheme ? .black : .white

        guard let navBar = (UIApplication.shared.delegate as? AppDelegate)?.rootNavController?.navigationBar else { return }
        navBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.projectFont(size: 22),
                                      NSAttributedString.Key.foregroundColor: useLightTheme ? UIColor.black : UIColor.white]
        navBar.tintColor = useLightTheme ? .black : .white
        navBar.barTintColor = useLightTheme ? .white : .black
        navBar.backgroundColor = useLightTheme ? .white : .black
    }
        
    deinit {
        styleObserver?.invalidate()
    }
}
