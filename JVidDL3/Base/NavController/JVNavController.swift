//
//  JVNavController.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 03/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVNavController: UINavigationController {
    
    private var styleObserver: NSKeyValueObservation?
    
    override init(navigationBarClass: AnyClass?, toolbarClass: AnyClass?) {
        super.init(navigationBarClass: navigationBarClass, toolbarClass: toolbarClass)
        
        styleObserver = UserDefaults.standard.observe(\.useLightContent, options: [.initial, .new], changeHandler: { [weak self] (defaults, change) in
            self?.setNeedsStatusBarAppearanceUpdate()
        })
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UserDefaults.standard.useLightContent ? UIStatusBarStyle.default : UIStatusBarStyle.lightContent
    }

    deinit {
        styleObserver?.invalidate()
    }
}
