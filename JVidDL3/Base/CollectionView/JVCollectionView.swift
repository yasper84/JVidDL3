//
//  JVCollectionView.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 02/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVCollectionView: UICollectionView {
    private struct Constants {
        static let SectionInset: UIEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    private var styleObserver: NSKeyValueObservation?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = Constants.SectionInset
        }
        
        observeStyle()
    }
    
    private func observeStyle() {
        styleObserver = UserDefaults.standard.observe(\.useLightContent, options: [.initial, .new], changeHandler: { [weak self] (defaults, change) in
            let useLightTheme = change.newValue == true
            self?.backgroundColor = useLightTheme ? .white : .black
        })
    }
    
    deinit {
        styleObserver?.invalidate()
    }
}
