//
//  JVBaseCell.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 09/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVBaseCell: UICollectionViewCell {
    class var identifier: String {
        return String(describing: self.self)
    }
}
