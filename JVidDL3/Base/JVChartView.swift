//
//  JVChartView.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 15/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVChartView: UIView {
    private struct Constants {
        static let OuterBorderWidth: CGFloat = 6
        static let InnerLineWidth: CGFloat = 3
    }
    
    var emptyColor: UIColor = .white
    var fillColor: UIColor = .black

    var percentage: Int = 0 {
        didSet {
            guard oldValue != percentage else { return }            
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        isOpaque = false
    }
    
    override func draw(_ rect: CGRect) {
        // Draw background
        let totalRadius = width/2
        drawCircle(startAngle: 0,
                   endAngle: 2 * .pi * totalRadius,
                   circleColor: .white,
                   radius: totalRadius)
        
        // Draw piechart
        var startAngle = -CGFloat.pi * 0.5
        for entry in [(emptyColor, 100 - percentage), (fillColor, percentage)] {
            let endAngle = startAngle + 2 * .pi * (CGFloat(entry.1) / 100)
            drawCircle(startAngle: startAngle, endAngle: endAngle, circleColor: entry.0, radius: totalRadius - Constants.OuterBorderWidth)
            startAngle = endAngle
        }

        // Draw border arc
        let borderRadius = totalRadius - Constants.InnerLineWidth
        drawCircle(startAngle: 0,
                   endAngle: 2 * .pi * borderRadius,
                   circleColor: .black,
                   radius: borderRadius,
                   lineWidth: Constants.InnerLineWidth)
    }
    
    private func drawCircle(startAngle: CGFloat, endAngle: CGFloat, circleColor: UIColor, radius: CGFloat, lineWidth: CGFloat? = nil) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        let viewCenter = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)

        let strokeLine = lineWidth != nil
        if let lineWidth = lineWidth {
            context.setStrokeColor(circleColor.cgColor)
            context.setLineWidth(lineWidth)
        } else {
            context.move(to: viewCenter)
            context.setFillColor(circleColor.cgColor)
        }
        
        context.addArc(center: viewCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)

        if strokeLine {
            context.strokePath()
        } else {
            context.fillPath()
        }
    }
}
