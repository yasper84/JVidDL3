//
//  JVStyleView.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 02/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVStyleView: JVView {
    
    @IBInspectable var invertColor: Bool = false
    private var styleObserver: NSKeyValueObservation?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        DispatchQueue.main.async {
            self.observeStyle()
        }
    }
    
    private func observeStyle() {
        styleObserver = UserDefaults.standard.observe(\.useLightContent, options: [.initial, .new], changeHandler: { [weak self] (defaults, change) in
            let useLightTheme = change.newValue == true
            
            let targetColor: UIColor
            if useLightTheme {
                targetColor = (self?.invertColor == true) ? .black : .white
            } else {
                targetColor = (self?.invertColor == true) ? .white : .black
            }
            self?.backgroundColor = targetColor
        })
    }
    
    deinit {
        styleObserver?.invalidate()
    }
}
