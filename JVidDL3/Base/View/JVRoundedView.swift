//
//  JVRoundedView.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 15/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVRoundedView: JVBorderedView {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = height/2
    }
}
