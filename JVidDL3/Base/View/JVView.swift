//
//  JVView.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 10/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVView: UIView {
    class var nibName: String {
        return String(describing: self.self)
    }
}
