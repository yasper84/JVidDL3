//
//  JVViewController.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 13/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVViewController: UIViewController {
    
    private struct Constants {
        static let IndicatorSize: CGFloat = 50
    }

    private let activityIndicator = NVActivityIndicatorView(frame: .zero, type: NVActivityIndicatorType.ballPulse, color: .white, padding: 0)
    private let activityView: UIView = {
        let actView = UIView(forAutoLayout: ())
        actView.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        actView.isHidden = true
        actView.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        return actView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(activityView)
        activityView.autoPinEdgesToSuperviewEdges()

        activityView.addSubview(activityIndicator)
        activityIndicator.autoCenterInSuperview()
        activityIndicator.autoSetDimensions(to: CGSize(width: Constants.IndicatorSize, height: Constants.IndicatorSize))
    }
    
    var indicateActivity: Bool = false {
        didSet {
            activityView.isHidden = !indicateActivity

            if indicateActivity {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
        }
    }
    
    func push(viewController: UIViewController) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate, let rootNC = appDelegate.rootNavController else { return }
        rootNC.pushViewController(viewController, animated: true)
    }
}
