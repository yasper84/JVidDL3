//
//  JVStyleButton
//  JVidDL3
//
//  Created by Jasper Siebelink on 03/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVStyleButton: UIButton {
    private var styleObserver: NSKeyValueObservation?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        DispatchQueue.main.async {
            self.observeStyle()
        }
    }
    
    private func observeStyle() {
        styleObserver = UserDefaults.standard.observe(\.useLightContent, options: [.initial, .new], changeHandler: { [weak self] (defaults, change) in
            let useLightTheme = change.newValue == true
            self?.tintColor = useLightTheme ? .black : .white
        })
    }
    
    deinit {
        styleObserver?.invalidate()
    }
}
