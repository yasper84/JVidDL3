//
//  JVBorderedButton.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 01/05/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVBorderedButton: JVLocalizedButton {
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
}
