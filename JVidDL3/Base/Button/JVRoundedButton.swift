//
//  JVRoundedButton.swift
//  JVidDL3
//
//  Created by Jasper Siebelink on 14/04/2018.
//  Copyright © 2018 Jasper Siebelink. All rights reserved.
//

import UIKit

class JVRoundedButton: JVBorderedButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = height/2
    }
}
